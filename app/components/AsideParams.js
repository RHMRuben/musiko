const AsideParams = {
	props: {app: Object},
	data: function () {
		return {
			version: VERSION,
			selectedLang: this.app.language,
			options: [
				{ text: 'العربية', value: 'ae' },
				{ text: '简体中文', value: 'cn' },
				{ text: 'Deutch', value: 'de' },
				{ text: 'English', value: 'en' },
				{ text: 'Esperanto', value: 'eo' },
				{ text: 'Español', value: 'es' },
				{ text: 'Français', value: 'fr' },
				{ text: 'Italiano', value: 'it' },
				{ text: '日本の', value: 'jp' },
				{ text: 'русский', value: 'ru' }
			],
			options2: [
				{ text: 'Dark', value: 'dark' },
				{ text: 'Deep blue (dark)', value: 'deepblue' },
				{ text: 'Forest (dark)', value: 'forest' },
				{ text: 'Purple (dark)', value: 'purple' },
				{ text: 'Neon (dark)', value: 'neon' },
				{ text: 'Greyscale (light)', value: 'greyscale' },
				{ text: 'Sand (light)', value: 'sand' },
				{ text: 'Sail (light)', value: 'sail' },
				{ text: 'Opaline (light)', value: 'opaline' },
				{ text: 'Amaranth (light)', value: 'amaranth' }
			]
		}
	},
	template: `
		<div class="params panel">
			<div class=title1 v-html="app.L.dossierMusical"></div>
			<div style="padding: 0 20px">
				<input style="display: none;" type="file" id="filepicker" name="fileList" webkitdirectory @change="changePath"/>
				<div class="bt" style="width:100%" @click="openDial" v-html="app.L.choisirDossier"></div>
			</div>

			<div class=title1 v-html="app.L.language"></div>
			<div style="padding: 0 20px">
				<select v-model="selectedLang" @change="changeLang">
					<option v-for="option in options" v-bind:value="option.value">{{ option.text }}</option>
				</select>
			</div>

			<div class=title1 v-html="app.L.theme"></div>
			<div style="padding: 0 20px">
				<select v-model="app.theme" @change="changeTheme">
					<option v-for="option in options2" v-bind:value="option.value">{{ option.text }}</option>
				</select>
			</div>

			<div class=title1 v-html="app.L.nbAlbums"></div>
			<div style="padding: 0 20px">
				<input type="number" v-model="app.nbLastAlbums" @change="changeConfigFile" min="0" max="27">
			</div>

			<div class=title1 v-html="app.L.informations"></div>
			<div style="padding: 0 20px">
				<p>{{ app.L.dossierMusical }}&nbsp;:<br><span class="bt btPath" @click="openFolder(app.collectionPath)">{{ app.collectionPath }}</span></p>
				<p>{{ app.L.dossierConfig }}&nbsp;:<br><span class="bt btPath" @click="openFolder(app.configPath)">{{ app.configPath }}</span></p>
				<p>{{ app.L.version }}&nbsp;:<br><a href="https://bitbucket.org/diatomee/musiko/src/master/VERSIONS.md" class="bt btPath">{{ version }}</a></p>
				<p>{{ app.L.aide }}&nbsp;:<br><strong>F1</strong> - <a href="https://bitbucket.org/diatomee/musiko/" class="bt btPath">https://bitbucket.org/diatomee/musiko/</a></p>
			</div>
		</div>
	`,
	methods: {
		openFolder: function (path) {
			shell.openItem(path)
		},
		changeConfigFile: function () {
			const conf = {
				collectionPath: this.app.collectionPath,
				language: this.app.language,
				theme: this.app.theme,
				nbLastAlbums: this.app.nbLastAlbums,
				version: VERSION
			}
			fs.writeFileSync(path.join(savePATH, 'config.json'), JSON.stringify(conf))
			return conf
		},
		changePath: function (e) {
			const
				aDirName = path.dirname(e.target.files[0].path),
				childFolder = path.dirname(e.target.files[0].webkitRelativePath).split(path.sep)[0],
				splitPath = aDirName.split(path.sep)
				index = splitPath.indexOf(childFolder)

			let realPath = [], i = 0
			while (i <= index) {
				realPath.push(splitPath[i])
				i++
			}
			realPath = realPath.join('/')

			this.app.collectionPath = realPath
			//changer aussi dans le json puis rafraichir la collection
			const config = this.changeConfigFile()
			/*
			fs.unlinkSync(path.join(savePATH, 'collection.json'))
			fs.unlinkSync(path.join(savePATH, 'genres.json'))
			fs.unlinkSync(path.join(savePATH, 'lastAlbums.json'))
			fs.unlinkSync(path.join(savePATH, 'playlists.json'))
			fs.unlinkSync(path.join(savePATH, 'years.json'))
			*/
			initCollection(config)
		},
		changeLang: function () {
			this.app.language = this.selectedLang
			this.app.L = lang[this.selectedLang]
			this.changeConfigFile()
		},
		changeTheme: function () {
			this.changeConfigFile()
			document.body.setAttribute('data-theme', this.app.theme)
		},
		openDial: function () {
			document.getElementById('filepicker').click()
		}
	}
}
