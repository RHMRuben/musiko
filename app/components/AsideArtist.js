const AsideArtist = {
	props: {app: Object},
	components: {Albums, Tracks},
	template: `
		<div v-if=app.inPan class=panel>
			<div v-if="artists.length > 0">
				<div v-for="a of artists">
					<div class=title1>{{a.artist}}</div>

					<div class=title2 v-if="a.res.albums.length > 0">{{a.res.albums.length}} <span class="iconStatic" v-html="$root.I.album"></span></div>

					<Albums :app=app :albums=a.res.albums find="no"></Albums>

					<div class=title2 v-if="a.res.tracks.length > 0">{{a.res.tracks.length}} <span class=iconStatic v-html=$root.I.track></span></div>
					<Tracks :app=app :tracks=a.res.tracks :search=false></Tracks>
				</div>
			</div>
			<div v-else>
				<em>La piste machin truc est sans artiste défini dans les métadonnées.</em>
			</div>
			<br><br>
		</div>
		<div v-else class=panel>
			<div style="padding: 20px;"><em>{{app.L.pasAlbum}}</em></div>
		</div>
	`,
	computed: {
		artists: function () {
			let arr = []
			if (this.app.inPan.artist) {
				for (let a of this.app.inPan.artist) {
					arr.push({
						artist: a,
						res: this.getArtworksOfArtist(a)
					})
				}
				return arr
			}
			if (this.app.inPan.track.artists && this.app.inPan.track.artists.length > 0) {
				for (let artist of this.app.inPan.track.artists) {
					arr.push({
						artist,
						res: this.getArtworksOfArtist(artist)
					})
				}
			}
			return arr
		}
	},
	methods: {
		getArtworksOfArtist: function(text) {
			let byGT = [], byGA = [], byGTok = [], byGAok = []
			for (const a of this.app.allAlbums) for (const d of a.discs) for (const t of d.tracks) {
				if (t.artists && t.artists.indexOf(text) !== -1) byGT.push({t, a})
			}
			/*	réunion des pistes ayant le même album et le même artwork
				byGA = [
					{artworkName, albumName, tracks: [{t, a}]},
					..
				]
			*/
			for (const f of byGT) {
				const i = byGA.findIndex(el => el.artworkName === f.a.artwork && el.albumName === f.a.album)
				if (i === -1) {
					//on ajoute un objet à byGA
					byGA.push({artworkName: f.a.artwork, albumName: f.a.album, tracks: [f]})
				} else {
					//on ajoute la piste à l'index
					byGA[i].tracks.push(f)
				}
			}
			/*
				ensuite on récupère le nombre de piste d'un album et on le compare au nombre de piste dans byGA.
				Si le compte y est, on met dans byGAok. S'il n'y est pas, on met dans byGTok.
			*/
			for (const a1 of byGA) {
				for (const a of this.app.allAlbums) {
					if (a1.artworkName === a.artwork && a1.albumName === a.album) {
						let nbTracks = 0
						for (const d of a.discs) nbTracks += d.tracks.length
						if (a1.tracks.length === nbTracks) {
							byGAok.push(a1.tracks[0].a)
						} else {
							for (const t of a1.tracks) byGTok.push(t)
						}
						continue
					}
				}
			}
			/*	Note :
				on pourrait améliorer cette fonction vu que tout est trié alphabétiquement. Si la lettre est après ce que l'on recherche, on passe à l'élément suivant.
				Note 2 : Cette fonction est très similaire à 2 autres qui sont dans AsideSearch !
			*/
			return {albums: byGAok, tracks: byGTok}
		}
	}
}
