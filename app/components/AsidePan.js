const Asidepan = {
	props: {app: Object, tabs: Array, tab: String},
	components: {AsideQueue, AsideSearch, AsideAlbum, AsideTrack, AsideArtist, AsideParams},
	template: `
		<aside>
			<nav>
				<div
					v-for="t in tabs"
					:key="t"
					:class="['tab', 'tab' + t, { active: tab === t}]"
					@click="$root.tab = t"
					v-html="$root.$data.I[t.toLowerCase()]"
				></div>
			</nav>
			<AsideQueue v-if="tab === tabs[0]" :app="app"></AsideQueue>
			<AsideSearch v-if="tab === tabs[1]" :app="app"></AsideSearch>
			<AsideAlbum v-if="tab === tabs[2]" :app="app"></AsideAlbum>
			<AsideTrack v-if="tab === tabs[3]" :app="app"></AsideTrack>
			<AsideArtist v-if="tab === tabs[4]" :app="app"></AsideArtist>
			<AsideParams v-if="tab === tabs[5]" :app="app"></AsideParams>
		</aside>
	`
}
