const Album1 = {
	props: {app: Object, album: Object, find: String},
	template: `
		<div
			@click.exact=play
			@click.ctrl.exact=show
			@click.right.exact=show
			class=searchResult
		>
			<img style="width:22%;margin-right:.5em;" :src=album.cover></img>
			<div v-if="find === 'no'">
				<div>{{album.album}}</div>
				<div>{{album.artwork}}</div>
			</div>
			<div v-if="find === 'both'">
				<div><b>{{album.album}}</b></div>
				<div><b>{{album.artwork}}</b></div>
			</div>
			<div v-if="find === 'album'">
				<div><b>{{album.album}}</b></div>
				<div>{{album.artwork}}</div>
			</div>
			<div v-if="find === 'artwork'">
				<div>{{album.album}}</div>
				<div><b>{{album.artwork}}</b></div>
			</div>
		</div>
	`,
	methods: {
		play: function() {
			this.app.playAlbum(this.album)
		},
		show: function() {
			const track = this.album.discs[0].tracks[0]
			this.app.inPan = {track, album: this.album}
			this.$root.tab = 'Album'
		}
	}
}
const Albums = {
	props: {app: Object, albums: Array, find: String},
	components: {Album1},
	template: `
	<div>
		<Album1 v-for="a, i of albums" :key="'album' + i" :app=app :album=a :find=find></Album1>
	</div>
	`
}