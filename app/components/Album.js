const Album = {
	props: {
		app: Object,
		album: Object,
		goto: Boolean,
		playlist: String,
		concise: Boolean	//abrège les noms d'album et réduit à 2 le nombre d'artistes cliquables
	},
	data: function () {
		return {isHover: false}
	},
	template: `
	<div style="padding: 20px">
		<div
			@mouseover=toggleHover
			@mouseout=toggleHover
			:class="{hover:isHover}"
			@click.exact=play
			@click.ctrl.exact=show
			@click.right.exact=show
		>
			<img :src=path>
			<div style="font-size:1.2em;margin-bottom:2px;margin-top:-2px;">
				<b v-html=getConcise></b>
			</div>
		</div>
		<span class=miniBt
			v-for="el of artworkAndArtists"
			v-html=el.text
			@click.exact=playArtists(el.artists)
			@click.ctrl.exact=goArtists(el.artists)
			@click.right.exact=goArtists(el.artists)
		></span>
	</div>
	`,
	computed: {
		nbTracks: function() { //non utilisé (donne le nombre de pistes de l'album)
			let i = 0
			for (const d of this.album.discs) i += d.tracks.length
			return i
		},
		path: function() {
			if (!this.album.cover) return 'app/img/noCover.svg'
			return this.album.cover
		},
		artworkAndArtists: function() {
			let arr = []
			const artwork = this.album.artwork
			for (let d of this.album.discs) for (let t of d.tracks) for (let a of t.artists) {
				if (arr.findIndex(el => el.text === a) === -1) arr.push({text: a, artists: [a]})
			}
			if (arr.length === 1 && arr[0].text === artwork) {
				//on a un seul artiste dans arr et il se trouve que c'est le nom de l'oeuvre
				if (!this.goto) return [{text: artwork, artists: [artwork]}]
				return [{
					text: '<span data-name=artwork>' + artwork + '</span>',
					artists: [artwork]
				}]
			}
			//if (arr.length > 2) arr = [arr[0], arr[1], ('+' + (arr.length - 2))]
			if (this.concise && arr.length > 2) {
				const newArr = arr.slice(2)
				let artists = []
				for (const el of newArr) {
					for (const a of el.artists) artists.push(a)
				}
				arr = [arr[0], arr[1], {text: '…', artists}]
			}
			arr.unshift({
				text: '<b' + (this.goto ? ' data-name=artwork' : '') + '>' + artwork + '</b>',
				artists: arr.map(el => el.artists[0])
			})
			return arr
		},
		getConcise: function() {
			if (!this.concise) return this.album.album
			if (this.album.album.length < 38) return this.album.album
			const a = this.album.album
			return a.substring(0, 35).trim() + '…'
		},
	},
	methods: {
		
		toggleHover: function() {this.isHover = !this.isHover},
		getArtists: function(album) { //utilisé dans play et show
			let artist = []
			for (const d of album.discs) {
				for (const t of d.tracks) {
					for (const a of t.artists) {
						if (artist.indexOf(a) === -1) artist.push(a)
					}
				}
			}
			return artist
		},
		play: function () {
			this.app.playAlbum(this.album)
			this.app.inPan = {
				track: this.album.discs[0].tracks[0],
				album: this.album,
				artist: this.getArtists(this.album)
			}
		},
		show: function () {
			this.app.inPan = {
				track: this.album.discs[0].tracks[0],
				album: this.album,
				artist: this.getArtists(this.album)
			}
			this.$root.tab = 'Album'
		},
		goArtists(artists) {
			this.app.inPan = {
				track: this.album.discs[0].tracks[0],
				album: this.album,
				artist: artists
			}
			this.$root.tab = 'Artist'
		},
		playArtists(artists) {
			for (const artist of artists) {
				for (const a of this.app.allAlbums) {
					for (const d of a.discs) {
						for (const t of d.tracks) {
							if (t.artists.indexOf(artist) !== -1) {
								this.app.playTrack({track: t, album: a})
							}
						}
					}
				}
			}
		}
	}
}