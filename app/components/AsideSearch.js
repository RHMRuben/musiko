const AsideSearch = {
	props: {app: Object},
	data: function () {
		return {
			fArtworks: [],
			fAlbums: [],
			fArtworksAlbums: [],
			fDiscs: [],
			fArtists: [],
			fTracks : [],
			fArtistsFull: [],
			//
			opened: 'search',
			//
			text: '',
			//
			byGenresA: [],
			byGenresT: [],
			byYearsA: [],
			byYearsT: []
		}
	},
	components: {Albums, Tracks},
	template: `
		<div class="search panel">
			<div class="title1 toggle" @click="changeOpened('index')">
				<div class="iconStatic noLeft" :class="{rotate: this.opened === 'index'}" v-html=this.$root.I.fold></div> {{app.L.oeuvreParIndex}}
			</div>
			<div v-if="opened === 'index'" class=indexs>
				<span v-for="c of getIndexs()" @click=goTo(c)>{{c}}</span>
			</div>

			<div class="title1 toggle" @click="changeOpened('search')">
				<div class="iconStatic noLeft" :class="{rotate: this.opened === 'search'}" v-html=this.$root.I.fold></div> {{app.L.rechercheGlobale}}
			</div>
			<div v-if="opened === 'search'">
				<div style="padding: 0 20px;">
					<p v-html="app.L.recherche"></p>
					<div style="display: flex;">
						<input type="text" v-focus v-model="text" @keyup.enter="searcher"/>
						<div v-if="text.length > 2" class=bt v-html="$root.I.search" @click="searcher"></div>
					</div>
					<p v-if="total > 0">{{ app.L.resultats }} <b>{{total}}</b></p>
				</div>
				<div v-if="fArtworks.length + fAlbums.length + fArtworksAlbums.length + fDiscs.length > 0" style="margin-top: 1.5em;">
					<div class=title2>{{fArtworks.length + fAlbums.length + fArtworksAlbums.length + fDiscs.length}} <span class="iconStatic" v-html="$root.I.album"></span></div>
					<Albums :app=app :albums=fArtworks find=artwork></Albums>
					<Albums :app=app :albums=fAlbums find=album></Albums>
					<Albums :app=app :albums=fArtworksAlbums find=both></Albums>
					<div v-if="fDiscs.length > 0">
						<div
							v-for="f of fDiscs"
							@click.exact=playDisc(f)
							@click.ctrl.exact=showTracks(f.a)
							@click.right.exact=showTracks(f.a)
							@click.middle.exact=showTracks(f.a)
							class=searchResult
						>
							<img v-if=f.d.disccover style="width:15%;margin-right:.5em;" :src=f.d.disccover>
							<div><b>{{f.d.disctitle}}</b><br>{{f.a.album}}</div>
						</div>
					</div>
				</div>
				<div v-if="fArtists.length > 0" style="margin-top: 1.5em;">
					<div class=title2>{{fArtists.length}} <span class=iconStatic v-html="$root.I.artist"></span></div>
					<div
						v-for="f of fArtists"
						@click.exact=playArtistTracks(f)
						@click.ctrl.exact=showArtist(f)
						@click.right.exact=showArtist(f)
						class=searchResult
					>
						<b>{{f.artist}}</b>
					</div>
				</div>
				<div v-if="fTracks.length > 0" style="margin-top: 1.5em;">
					<div class=title2>{{fTracks.length}} <span class="iconStatic" v-html="$root.I.track"></span></div>
					<Tracks :app=app :tracks=fTracks :search=true></Tracks>
				</div>
			</div>


			<div class="title1 toggle" @click="changeOpened('genres')">
				<div class="iconStatic noLeft" :class="{rotate: this.opened === 'genres'}" v-html=this.$root.I.fold></div> {{app.L.genres}}
			</div>
			<div v-if="opened === 'genres'" class="tags">
				<span
					v-for="(g, index) of app.genres"
					@click="getArtworksOfGenre(g)"
				>{{ g }}</span>
			</div>
			<div v-if="opened === 'genres' && byGenresA.length + byGenresT.length > 0">
				<div class=title2 v-if="byGenresA.length > 0">{{byGenresA.length}} <span class="iconStatic" v-html="$root.I.album"></span></div>
				<Albums :app=app :albums=byGenresA find=no></Albums>
				<div class=title2 v-if="byGenresT.length > 0">{{byGenresT.length}} <span class="iconStatic" v-html="$root.I.track"></span></div>
				<Tracks :app=app :tracks="byGenresT" :search=true></Tracks>
			</div>
			<div class="title1 toggle" @click="changeOpened('years')">
				<div class="iconStatic noLeft" :class="{rotate: this.opened === 'years'}" v-html=this.$root.I.fold></div> {{app.L.annees}}
			</div>
			<div v-if="opened === 'years'" class="tags">
				<span
					v-for="(y, index) of app.years"
					@click="getArtworksOfYear(y)"
				>{{ y }}</span>
			</div>
			<div v-if="opened === 'years' && byYearsA.length + byYearsT.length > 0">
				<div class=title2 v-if="byYearsA.length > 0">{{byYearsA.length}} <span class=iconStatic v-html=$root.I.album></span></div>
				<Albums :app=app :albums=byYearsA find=no></Albums>
				<div class=title2 v-if="byYearsT.length > 0">{{byYearsT.length}} <span class=iconStatic v-html=$root.I.track></span></div>
				<Tracks :app=app :tracks="byYearsT" :search=true></Tracks>
			</div>
			<br>
		</div>
	`,
	directives: {
		focus: {inserted: function (el) {el.focus()}}
	},
	computed: {
		total: function () {
			const len = this.fArtworks.length + this.fAlbums.length + this.fArtworksAlbums.length + this.fDiscs.length + this.fArtists.length + this.fTracks.length
			return len
		}
	},
	methods: {
		getArtworksOfGenre: function(text) {
			let byGT = [], byGA = [], byGTok = [], byGAok = []
			for (const a of this.app.allAlbums) for (const d of a.discs) for (const t of d.tracks) {
				if (t.genres && t.genres.indexOf(text) !== -1) byGT.push({t, a})
			}
			/*	réunion des pistes ayant le même album et le même artwork
				byGA = [
					{artworkName, albumName, tracks: [{t, a}]},
					..
				]
			*/
			for (const f of byGT) {
				const i = byGA.findIndex(el => el.artworkName === f.a.artwork && el.albumName === f.a.album)
				if (i === -1) {
					//on ajoute un objet à byGA
					byGA.push({artworkName: f.a.artwork, albumName: f.a.album, tracks: [f]})
				} else {
					//on ajoute la piste à l'index
					byGA[i].tracks.push(f)
				}
			}
			/*
				ensuite on récupère le nombre de piste d'un album et on le compare au nombre de piste dans byGA.
				Si le compte y est, on met dans byGAok. S'il n'y est pas, on met dans byGTok.
			*/
			for (const a1 of byGA) {
				for (const a of this.app.allAlbums) {
					if (a1.artworkName === a.artwork && a1.albumName === a.album) {
						let nbTracks = 0
						for (const d of a.discs) nbTracks += d.tracks.length
						if (a1.tracks.length === nbTracks) {
							byGAok.push(a1.tracks[0].a)
						} else {
							for (const t of a1.tracks) byGTok.push(t)
						}
						continue
					}
				}
			}
			/*	Note :
				on pourrait améliorer cette fonction vu que tout est trié alphabétiquement. Si la lettre est après ce que l'on recherche, on passe à l'élément suivant.
			*/
			this.byGenresT = byGTok
			this.byGenresA = byGAok
		},
		getArtworksOfYear: function(text) {
			let byGT = [], byGA = [], byGTok = [], byGAok = []
			for (const a of this.app.allAlbums) for (const d of a.discs) for (const t of d.tracks) {
				if (t.year && t.year === text) byGT.push({t, a})
			}
			/*	réunion des pistes ayant le même album et le même artwork
				byGA = [
					{artworkName, albumName, tracks: [{t, a}]},
					..
				]
			*/
			for (const f of byGT) {
				const i = byGA.findIndex(el => el.artworkName === f.a.artwork && el.albumName === f.a.album)
				if (i === -1) {
					//on ajoute un objet à byGA
					byGA.push({artworkName: f.a.artwork, albumName: f.a.album, tracks: [f]})
				} else {
					//on ajoute la piste à l'index
					byGA[i].tracks.push(f)
				}
			}
			/*
				ensuite on récupère le nombre de piste d'un album et on le compare au nombre de piste dans byGA.
				Si le compte y est, on met dans byGAok. S'il n'y est pas, on met dans byGTok.
			*/
			for (const a1 of byGA) {
				for (const a of this.app.allAlbums) {
					if (a1.artworkName === a.artwork && a1.albumName === a.album) {
						let nbTracks = 0
						for (const d of a.discs) nbTracks += d.tracks.length
						if (a1.tracks.length === nbTracks) {
							byGAok.push(a1.tracks[0].a)
						} else {
							for (const t of a1.tracks) byGTok.push(t)
						}
						continue
					}
				}
			}
			/*	Note :
				on pourrait améliorer cette fonction vu que tout est trié alphabétiquement. Si la lettre est après ce que l'on recherche, on passe à l'élément suivant.
				Note2 : Pratiquement tout le code de cette fonction est identique à celui de getArtworksOfGenre() !
			*/
			this.byYearsT = byGTok
			this.byYearsA = byGAok
		},
		changeOpened: function(text) {
			if (text === this.opened) {
				this.opened = ''
				return
			}
			this.opened = text
		},
		showTracks: function(album) {
			const track = album.discs[0].tracks[0]
			this.app.inPan = {track, album}
			this.$root.tab = 'Album'
		},
		playDisc: function(discAndAlbum) {
			const {d, a} = discAndAlbum
			const index = a.discs.findIndex(disc => d.disctitle === disc.disctitle)
			this.app.playAlbum(a, index)
			this.app.showPause = true
		},
		playArtistTracks: function(track) {
			for (const t of this.fArtistsFull) {
				if (t.artist === track.artist) this.app.playTrack({track: t.t, album: t.a})
			}
		},
		showArtist: function(track) {
			this.app.inPan = {track: track.t, album: track.a, artist: [track.artist]}
			this.$root.tab = 'Artist'
		},
		searcher: function () {
			this.fArtworks = []
			this.fAlbums = []
			this.fArtworksAlbums = []
			this.fDiscs = []
			this.fArtists = []
			this.fTracks = []
			this.fArtistsFull = []
			const char = this.text.toUpperCase()
			if (char.length < 3) return
			for (const a of this.app.allAlbums) for (const d of a.discs) {
				for (const t of d.tracks) {
					if (a.artwork !== t.artists.join(', ') && a.artwork.toUpperCase().search(char) !== -1) {
						if (this.fArtworks.findIndex(el => el.artwork === a.artwork && el.album === a.album) === -1) this.fArtworks.push(a)
					}
					if (a.album.toUpperCase().search(char) !== -1) {
						if (this.fAlbums.findIndex(el => el.album === a.album) === -1) this.fAlbums.push(a)
					}
					for (const artist of t.artists) {
						if (artist.toUpperCase().search(char) !== -1) {
							if (this.fArtists.findIndex(el => el.artist === artist) === -1) this.fArtists.push({t, a, artist})
							this.fArtistsFull.push({t, a, artist})
						}
					}
					if (t.title.toUpperCase().search(char) !== -1) {
						this.fTracks.push({t, a})
					}
				}
				if (d.disctitle && d.disctitle.toUpperCase().search(char) !== -1) {
					this.fDiscs.push({d, a})
				}
			}
			let j = 0
			for (let f of this.fArtworks) {
				const i = this.fAlbums.findIndex(el => el.artwork === f.artwork && el.album === f.album)
				if (i !== -1) {
					this.fArtworksAlbums.push(this.fAlbums[i])
					this.fArtworks.splice(j, 1)
					this.fAlbums.splice(i, 1)
				}
				j++
			}
			j = 0
			for (let f of this.fAlbums) {
				const i = this.fArtworks.findIndex(el => el.artwork === f.artwork && el.album === f.album)
				if (i !== -1) {
					this.fArtworksAlbums.push(this.fArtworks[i])
					this.fAlbums.splice(j, 1)
					this.fArtworks.splice(i, 1)
				}
				j++
			}
		},
		getIndexs: function () {
			let chars = []
			for (const a of this.app.albums) {
				const c = a.artwork.split('')[0].toUpperCase()
				if (chars.indexOf(c) === -1) chars.push(c)
			}
			return chars
		},
		goTo: function (char) {
			const elts = document.body.querySelectorAll("[data-name='artwork']")
			let go = false
			for (const e of [... elts]) {
				if (e.textContent.split('')[0].toUpperCase() === char) {
					if (!go) {
						e.parentNode.parentNode.scrollIntoView()
						go = true
					}
					e.classList.add('blink')
					const i = setInterval(() => {
						e.classList.remove('blink')
						clearInterval(i)
					}, 800)
				}
			}
		}
	}
}
