const Track = {
	props: {app: Object, track: Object, album: Object, search: Boolean, cover: Boolean, remove: Boolean},
	template: `
		<div
			@click.exact="remove ? rm({track, album}) : play(track, album)"
			@click.ctrl.exact="show(track, album)"
			@click.right.exact="show(track, album)"
			class=searchResult
			:class="{read: isRead, twoCol: (track.albumRef && track.albumRef.cover), remove}"
		>
			<div v-if=isRead class=pointerRead></div>
			<img v-if="track.albumRef && track.albumRef.cover" style="width:15%;margin-right:.5em;" :src=track.albumRef.cover>
			<img v-else-if="cover && album.cover" style="width:15%;margin-right:.5em;" :src=album.cover>
			<div>
				<b v-if=search>{{track.title}}</b>
				<div v-else>{{track.title}}</div>
				<div class=less>
					{{track.artists.join(', ')}}&nbsp;· {{app.getTime(track.duration)}}
				</div>
			</div>
		</div>
	`,
	computed: {
		isRead: function () {
			return this.app.past.length > 0
				&& this.app.past[0].track.title === this.track.title
				&& this.app.past[0].track.track === this.track.track
				&& this.app.past[0].track.duration === this.track.duration
		}
	},
	methods: {
		rm: function (q) {
			console.log(q)
			this.app.queue.splice(this.app.queue.findIndex(e => e.track.path === q.track.path), 1)
		},
		play: function(track, album) {
			this.app.playTrack({track, album})
		},
		show: function(track, album) {
			this.app.inPan = {track, album}
			this.$root.tab = 'Track'
		}
	}
}
const Tracks = {
	props: {app: Object, tracks: Array, search: Boolean, remove: Boolean},
	//une track a la forme {t, a}
	components: {Track},
	template: `
	<div>
		<Track
			v-for="f, i of tracks"
			:key="'track' + i"
			:app=app
			:track=f.t
			:album=f.a
			:search=search
			:remove=remove
			:cover=cover(tracks)
		></Track>
	</div>
	`,
	methods: {
		cover: function(tracks) {
			if (this.$root.tab !== 'Album') return true
			let covers = []
			for (const t of tracks) {
				if (covers.indexOf(t.a.cover) === -1) {
					covers.push(t.a.cover)
				}
			}
			return covers.length > 1 //si plus d'une couverture alors on veut les voir, sinon, c'est que c'est celle de l'album*/
		}
	}
}