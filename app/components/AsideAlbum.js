const AsideAlbum = {
	props: {app: Object},
	data: function () {
		return {}
	},
	components: {Album, Tracks},
	template: `
		<div v-if=app.inPan class=panel>
			<Album :app=app :album=app.inPan.album :goto=false playlist=no :concise=false></Album>
			<div v-for="d, index of app.inPan.album.discs">
				<div class="title1 toggle" v-if="d.disc && app.inPan.album.discs.length > 1" @click=playDisc(index) style="cursor: pointer;">
					<img v-if=d.disccover :src=d.disccover @click=playDisc(index)>
					<span v-if=d.disctitle :title="app.L.disque + ' ' + d.disc">{{d.disctitle}}</span>
					<span v-else>{{app.L.disque}} {{d.disc}}</span>
				</div>
				<Tracks :app=app :tracks=tracks(d.tracks) :search=false></Tracks>
			</div><br><br>
		</div>
		<div v-else class=panel>
			<div style="padding: 20px;"><em>{{app.L.pasAlbum}}</em></div>
		</div>
	`,
	methods: {
		playDisc: function(index) {
			this.app.playAlbum(this.app.inPan.album, index)
			this.app.showPause = true
		},
		tracks: function(tracks) {
			return tracks.map(t => ({t, a: this.app.inPan.album}))
		}
	}
}
