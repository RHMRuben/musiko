const lang = {
	fr: {
		creationCollection: 'Création de la collection musicale.',
		musiquesAnalysees: 'musiques analysées.',
		pasAlbum: 'Aucun album trouvé. Le chemin vers le dossier des musiques peut être changé dans les paramètres.',
		//panneau recherche
		oeuvreParIndex: 'Œuvre par index',
		rechercheGlobale: 'Recherche globale',
		resultats: 'Résultats :',
        recherche: 'Nom d’album, d’œuvre, de disque, d’artiste ou de titre de piste <span class=less>(3 caractères minimum)</span>.',
        //panneau album
        disque: 'Disque',
		//panneau file d'attente
		fileAttenteVide: 'La file d’attente est vide.',
		vider: 'Vider',
		historiqueVide: 'L’historique est vide.',
		pisteAttente: (nb, time) => `${nb} piste${nb > 1 ? 's' : ''} en attente (${time}).`,
		pisteLancee: nb => nb === 1 ? 'Une piste déjà lancée.' : nb + ' pistes déjà lancées.',
		//panneau filtres
		genres: 'Genres',
		annees: 'Années',
		retirerFiltres: 'Retirer tous les filtres',
		//panneau params
        dossierMusical: 'Dossier musical',
        dossierConfig: 'Dossier de configuration',
		choisirDossier: 'Choisir un dossier',
		informations: 'Informations',
		version: 'Version de Musiko',
		aide: 'Documentation',
        language: 'Langue',
        theme: 'Apparence',
        nbAlbums: "Nombre d'albums des dernières écoutes",
        eteindre: "Programmer l'extinction"
	},
	cn: {
		creationCollection: '创建音乐收藏。',
        musiquesAnalysees: '音乐分析。',
        pasAlbum: '找不到专辑。 音乐文件夹的路径可以在设置中更改。',
        //panneau recherche
        oeuvreParIndex: '按索引工作',
        rechercheGlobale: '全球搜寻',
        resultats: '结果：',
        recherche: '专辑，作品，艺术家或曲目标题名称。',
        //panneau album
        disque: '碟片',
        //panneau file d'attente
        fileAttenteVide: '队列为空。',
        vider: '空',
        historiqueVide: '历史记录是空的。',
        pisteAttente: (nb, time) => `${nb} + 音乐等待(${time})。`,
        pisteLancee: nb => `${nb > 1 ? '推出'+nb+'种音乐' : '音乐发布'}。`,
        //panneau filtres
        genres: '音乐流派',
        annees: '岁月',
        retirerFiltres: '删除所有过滤器',
        //panneau params
        dossierMusical: '音乐唱片',
        dossierConfig: '配置文件夹',
        choisirDossier: '选择一个音乐文件夹',
        informations: '信息',
        version: 'Musiko版本',
        aide: '文件',
        language: '语言',
        theme: '出现',
        nbAlbums: "最近一次聆听的专辑数量",
        eteindre: "计划关闭"
	},
	de: {
		creationCollection: 'Erstellung der Musiksammlung.',
        musiquesAnalysees: 'Musik analysiert.',
        pasAlbum: 'Kein Album gefunden. Der Pfad zum Musikalische Aufzeichnung kann in den Einstellungen geändert werden.',
        //panneau recherche
        oeuvreParIndex: 'Arbeit nach Index',
        rechercheGlobale: 'Globale Suche',
        resultats: 'Ergebnisse :',
        recherche: 'Name des Albums, der Arbeit, des Künstlers oder des Titels.',
        //panneau album
        disque: 'Disc',
        //panneau file d'attente
        fileAttenteVide: 'Die Warteschlange ist leer.',
        vider: 'Leeren Sie',
        historiqueVide: 'Die Geschichte ist leer.',
        pisteAttente: (nb, time) => `${nb} Track${nb > 1 ? 's' : ''} warren (${time}).`,
        pisteLancee: nb => nb === 1 ? 'Ein Track wurde bereits gestartet.' : nb + ' Tracks bereits gestartet.',
        //panneau filtres
        genres: 'Musikgenres',
        annees: 'Jahre',
        retirerFiltres: 'Entfernen Sie alle Filter',
        //panneau params
        dossierMusical: 'Musikalische Aufzeichnung',
        dossierConfig: 'Konfigurationsordner',
        choisirDossier: 'Wählen Sie einen Aufzeichnung',
        informations: 'Informationen',
        version: 'Version',
        aide: 'Dokumentation',
        language: 'Sprache',
        theme: 'Aussehen',
        nbAlbums: "Anzahl der Alben vom letzten Hören",
        eteindre: "Planen Sie das Herunterfahren"
	},
	en: {
		creationCollection: 'Building music collection.',
		musiquesAnalysees: 'tracks analyzed.',
		pasAlbum: 'No album found. The path to the music folder can be changed in the settings.',
		//panneau recherche
		oeuvreParIndex: 'Work by index',
		rechercheGlobale: 'Global search',
		resultats: 'Results:',
        recherche: 'Album, work, artist or track title name.',
        //panneau album
        disque: 'Disk',
		//panneau file d'attente
		fileAttenteVide: 'The queue is empty.',
		vider: 'Empty',
		historiqueVide: 'The history is empty.',
		pisteAttente: (nb, time) => `${nb} track${nb > 1 ? 's' : ''} pending (${time}).`,
		pisteLancee: nb => nb === 1 ? 'A track already launched.' : nb + ' tracks already launched.',
		//panneau filtres
		genres: 'Genres',
		annees: 'Years',
		retirerFiltres: 'Remove all filters',
		//panneau params
        dossierMusical: 'Music folder',
        dossierConfig: 'Configuration folder',
		choisirDossier: 'Choose a folder',
		informations: 'Informations',
		version: 'Version',
		aide: 'Documentation',
        language: 'Language',
        theme: 'Appearance',
		nbAlbums: "Number of albums from last listens",
        eteindre: "Schedule shutdown"
	},
	eo: {
		creationCollection: 'Kreo de la muzika kolekto.',
		musiquesAnalysees: 'muziko analizita.',
		pasAlbum: 'Neniu albumo trovita. La vojo al la muzika dosierujo povas esti ŝanĝita en la agordoj.',
		//panneau recherche
		oeuvreParIndex: 'Labori per indekso',
		rechercheGlobale: 'Serĉu ĉiujn',
		resultats: 'Rezultoj :',
        recherche: 'Albumo, verko, artisto aŭ titola nomo.',
        //panneau album
        disque: 'Disko',
		//panneau file d'attente
		fileAttenteVide: 'La vosto estas malplena.',
		vider: 'Malplenigi',
		historiqueVide: 'La historio estas malplena.',
		pisteAttente: (nb, time) => `${nb} trako${nb > 1 ? 'j' : ''} pritraktata (${time}).`,
		pisteLancee: nb => nb === 1 ? 'Trako jam lanĉita.' : nb + ' trakoj jam lanĉitaj.',
		//panneau filtres
		genres: 'Ĝenroj',
		annees: 'Jaroj',
		retirerFiltres: 'Forigu ĉiujn filtrilojn',
		//panneau params
        dossierMusical: 'Muzika dosierujo',
        dossierConfig: 'Agordo-dosierujo',
		choisirDossier: 'elektu dosierujon',
		informations: 'Novaĵoj',
		version: 'versio',
		aide: 'Dokumentado',
        language: 'Lingvo',
        theme: 'Aspekto',
		nbAlbums: "Nombro de albumoj de la lastaj aŭskultiloj",
        eteindre: "Horara halto"
	},
	es: {
		creationCollection: 'Creación de la colección musical.',
		musiquesAnalysees: 'música analizada.',
		pasAlbum: 'No se ha encontrado ningún álbum. La ruta a la carpeta de música se puede cambiar en la configuración.',
		//panneau recherche
		oeuvreParIndex: 'Trabajar por indice',
		rechercheGlobale: 'Búsqueda global',
		resultats: 'Resultados:',
        recherche: 'Nombre del título del álbum, obra, artista o pista.',
        //panneau album
        disque: 'Disco',
		//panneau file d'attente
		fileAttenteVide: 'La cola está vacía.',
		vider: 'Vaciar',
		historiqueVide: 'La historia está vacía.',
		pisteAttente: (nb, time) => `${nb} pista${nb > 1 ? 's' : ''} pendiente (${time}).`,
		pisteLancee: nb => nb === 1 ? 'Una pista ya lanzada.' : nb + ' pistas ya lanzadas.',
		//panneau filtres
		genres: 'Tipos',
		annees: 'Años',
		retirerFiltres: 'Eliminar todos los filtros',
		//panneau params
        dossierMusical: 'Carpeta de musica',
        dossierConfig: 'Carpeta de configuración',
		choisirDossier: 'Elige una carpeta',
		informations: 'Información',
		version: 'Versión',
		aide: 'Documentación',
        language: 'Lengua',
        theme: 'Apariencia',
		nbAlbums: "Número de álbumes de las últimas escuchas",
        eteindre: "Programar apagado"
	},
	it: {
		creationCollection: 'Creazione della collezione musicale.',
        musiquesAnalysees: 'musica analizzata.',
        pasAlbum: 'Nessun album trovato. Il percorso della cartella della musica può essere modificato nelle impostazioni.',
        //panneau recherche
        oeuvreParIndex: 'Lavora per indice',
        rechercheGlobale: 'Ricerca globale',
        resultats: 'risultati:',
        recherche: 'Nome del titolo dell’album, dell’opera, dell’artista o del brano.',
        //panneau album
        disque: 'Disco',
        //panneau file d'attente
        fileAttenteVide: 'La coda è vuota.',
        vider: 'svuotare',
        historiqueVide: 'La storia è vuota',
        pisteAttente: (nb, time) => `${nb} tracc${nb > 1 ? 'e' : 'ia'} in sospeso (${time}).`,
        pisteLancee: nb => nb === 1 ? 'Una traccia già lanciata.' : nb + ' tracce già lanciate.',
        //panneau filtres
        genres: 'Generi musicali',
        annees: 'Anni',
        retirerFiltres: 'Rimuovi tutti i filtri',
        //panneau params
        dossierMusical: 'Disco musicale',
        dossierConfig: 'Cartella di configurazione',
        choisirDossier: 'Scegli uno disco',
        informations: 'informazioni',
        version: 'Versione Musiko',
        aide: 'Documentazione',
        language: 'Lingua',
        theme: 'Aspetto',
        nbAlbums: "Numero di album degli ultimi ascolti",
        eteindre: "Pianifica l'arresto"
	},
	jp: {
		creationCollection: '音楽コレクションの作成。',
        musiquesAnalysees: '分析された音楽。',
        pasAlbum: 'アルバムが見つかりません。 音楽フォルダへのパスは、設定で変更できます。',
        //panneau recherche
        oeuvreParIndex: 'インデックスで作業する',
        rechercheGlobale: 'グローバル検索',
        resultats: '結果：',
        recherche: 'アルバム、作品、アーティスト、またはトラックのタイトル名。',
        //panneau album
        disque: 'ディスク',
        //panneau file d'attente
        fileAttenteVide: 'キューは空です。',
        vider: '空',
        historiqueVide: '履歴は空です。',
        pisteAttente: (nb, time) => `${nb > 1 ? '保留中のトラック': '保留中の'+nb+'トラック'}（${time}）。`,
        pisteLancee: nb => nb === 1 ? 'すでに発売されたトラック。' : nb + ' 既に発売されたトラック。',
        //panneau filtres
        genres: '音楽ジャンル',
        annees: '年',
        retirerFiltres: 'すべてのフィルターを削除',
        //panneau params
        dossierMusical: 'ミュージカルレコード',
        dossierConfig: '設定フォルダ',
        choisirDossier: 'フォルダーを選択',
        informations: '情報',
        version: 'ムシコバージョン',
        aide: 'ドキュメンテーション',
        language: '言語',
        theme: '外観',
        nbAlbums: "最後に聴いたアルバムの数",
        eteindre: "シャットダウンのスケジュール"
	},
	ru: {
		creationCollection: 'Создание музыкальной коллекции.',
        musiquesAnalysees: 'музыка анализируется.',
        pasAlbum: 'Альбом не найден. Путь к папке с музыкой можно изменить в настройках.',
        //panneau recherche
        oeuvreParIndex: 'Работа по индексу',
        rechercheGlobale: 'Глобальный поиск',
        resultats: 'Результаты:',
        recherche: 'Название альбома, работы, исполнителя или трека.',
        //panneau album
        disque: 'диск',
        //panneau file d'attente
        fileAttenteVide: 'Очередь пуста.',
        vider: 'очистить',
        historiqueVide: 'История пуста.',
        pisteAttente: (nb, time) => `${nb} ${nb > 1 ? 'трекa' : 'трек'} в ожидающий (${time}).`,
        pisteLancee: nb => nb === 1 ? '1 Трек уже запущен.' : nb + ' треки уже запущены.',
        //panneau filtres
        genres: 'музыкальные жанры',
        annees: 'лет',
        retirerFiltres: 'Удалить все фильтры',
        //panneau params
        dossierMusical: 'музыкальная запись',
        dossierConfig: 'Папка конфигурации',
        choisirDossier: 'Выберите запись',
        informations: 'информация',
        version: 'Версия Musiko',
        aide: 'документация',
        language: 'язык',
        theme: 'внешность',
        nbAlbums: "Количество альбомов с последнего прослушивания",
        eteindre: "Расписание выключения"
	},
	ae: {
		creationCollection: '.إنشاء المجموعة الموسيقية',
        musiquesAnalysees: '.الموسيقى تحليلها',
        pasAlbum: 'لم يتم العثور على ألبوم. يمكن تغيير المسار إلى مجلد الموسيقى في الإعدادات',
        //panneau recherche
        oeuvreParIndex: 'العمل حسب الفهرس',
        rechercheGlobale: 'البحث العالمي',
        resultats: ':النتائج',
        recherche: '.اسم الألبوم أو العمل أو الفنان أو المسار',
        //panneau album
        disque: 'القرص',
        //panneau file d'attente
        fileAttenteVide: '.قائمة الانتظار فارغة',
        vider: 'إفراغ',
        historiqueVide: '.التاريخ فارغ',
        pisteAttente: (nb, time) => `${nb > 1 ? 'المسارات' + ' المعلقة' + nb : 'مسار واحد معلق'} (${time})`,
        pisteLancee: nb => nb === 1 ? '.مسار تم إطلاقه بالفعل' : nb + ' .المسارات بدأت بالفعل',
        //panneau filtres
        genres: 'الأنواع الموسيقية',
        annees: 'سنوات',
        retirerFiltres: 'إزالة جميع المرشحات',
        //panneau params
        dossierMusical: 'سجل الموسيقية',
        dossierConfig: 'مجلد التكوين',
        choisirDossier: 'اختيار مجلد',
        informations: 'معلومات',
        version: 'نسخة',
        aide: 'توثيق',
        language: 'لغة',
        theme: 'المظهر',
        nbAlbums: "عدد الألبومات من آخر يستمع",
        eteindre: "جدولة إيقاف التشغيل"
	}
}