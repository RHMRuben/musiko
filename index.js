const isDebug = true //show/hide devTools

const { app, Menu, BrowserWindow, ipcMain, shell } = require('electron')
const fs = require('fs')
const path = require('path')
const klawSync = require('klaw-sync')
const mm = require('music-metadata')

const isMac = process.platform === 'darwin'

const template = [
  // { role: 'appMenu' }
  ...(isMac ? [{
    label: app.name,
    submenu: [
      { role: 'about' },
      { type: 'separator' },
      { label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:" },
      { label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
      { label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" },
      { label: "Select All", accelerator: "CmdOrCtrl+A", selector: "selectAll:" },
      { type: 'separator' },
      { role: 'services' },
      { type: 'separator' },
      { role: 'hide' },
      { role: 'hideothers' },
      { role: 'unhide' },
      { type: 'separator' },
      { role: 'quit' }
    ]
  }] : [])
]

const menu = Menu.buildFromTemplate(template)
Menu.setApplicationMenu(menu)

let win
function createWindow () {
	win = new BrowserWindow({
		width: 1200, minWidth: 600,
		height: 800, minHeight: 400,
		backgroundColor: '#080808',
		icon: 'assets/icon.png',
		webPreferences: {nodeIntegration: true},
		maximized: false,
		center: true
	})
	win.loadFile('index.html')
	win.setMenu(null)
	if (isDebug) win.webContents.openDevTools()
	win.on('closed', () => {win = null})
	win.webContents.on('will-navigate', (event, url) => {
		event.preventDefault()
		shell.openExternal(url)
	});
}
app.on('ready', createWindow)
app.on('window-all-closed', () => {if (!isMac) app.quit()})
app.on('activate', () => {if (win === null) createWindow()})
app.allowRendererProcessReuse = true






ipcMain.on('initCollection', (event, config, savePATH) => {
	const audio_exts = ['mp3','flac','wav','ogg','mp4','wma','oga','opus','m4a']
	function getMostFrequentValue(arr) {
		let counts = {}, compare = 0, mostFrequent
		for (let i = 0, len = arr.length; i < len; i++) {
			counts.word = !counts.word ? 1 : counts.word ++
			if (counts.word > compare) {
				compare = counts.word
				mostFrequent = arr[i]
			}
		}
		return mostFrequent
	}
	function klaw(dir, exts) {
		//traverse dir and return paths with exts
		return klawSync(dir, {
			traverseAll: true,
			filter: el => exts.indexOf(path.extname(el.path).toLowerCase().substring(1)) > -1
		}).map(p => p.path) //map améliorable
	}
	async function extendAudioFiles(files) {
		let nb = files.length, i = 0, musics = [], logMusics = []
		for (i; i < nb; i++) {
			const f = files[i]
			const m = await mm.parseFile(f)
			const mc = m.common
			const data = {
				path: f.replace(config.collectionPath, ''),
				duration: m.format.duration,
				album: mc.album,
				title: mc.title,
				year: mc.year
			}
			data.disc = mc.disk.no ? mc.disk.no : 0
			if (mc.track.no) data.track = mc.track.no
			if (mc.picture) data.pictures = mc.picture
			if (mc.discsubtitle) data.disctitle = mc.discsubtitle
			if (mc.albumartist) {
				data.artwork = mc.albumartist
				data.artists = [mc.albumartist]
			}
			if (mc.artist) {
				data.artists = mc.artist.split(', ')
				if (!mc.albumartist) data.artwork = data.artists.join(', ')
			}
			// tri des bonnes et mauvaises musiques
			if (data.album && data.artwork && data.title) { // si c'est bon...
				//... on ajoute les éventuels genres,
				if (mc.genre && mc.genre.length > 0) data.genres = mc.genre[0].split(', ').filter(g => g.trim() !== '')
				//... les éventuelles paroles,
				const bname = path.basename(data.path, path.extname(data.path))
				if (mc.lyrics) data.lyrics = mc.lyrics.join('')
				const lyricsFiles = klawSync(config.collectionPath + path.dirname(data.path), {
					traverseAll: true, filter: item => {
						const ext = path.extname(item.path).toLowerCase().substring(1)
						return ['lyrics'].indexOf(ext) !== -1
					}
				})
				for (let f of lyricsFiles) {
					if (path.basename(f.path) === bname + '.lyrics') {
						data.lyrics = fs.readFileSync(f.path, 'utf-8')
						break
					}
				}
				//... on repère si la musique est dans une playlist et on étoffe le tableau lists,
				/*for (let l of lists) {
					//const index = l.t.findIndex(path => path === data.path.normalize())
					const index = l.t.indexOf(data.path.normalize())
					if (index !== -1) l.t[index] = data
				}*/
				//... puis on ajoute au tableau
				musics.push(data)
			} else logMusics.push('- ' + data.path)
			win.webContents.send('info' , {nb: i+1, total: nb})
		}
		/*// Nettoyage du tableau des playlists
		let listsTmp = [], goodT = []
		for (const l of lists) {
			for (const t of l.t) t.artwork ? goodT.push(t) : logPath.push(t)
			if (goodT.length > 0) { 
				listsTmp.push({n: l.n, cover: l.cover, t: goodT})
				goodT = []
			} 
		}
		lists = listsTmp*/
		
		// Création des albums (on organise les pistes dans des disques, dans des albums)
		let albums = [], coverArr = [], disccoverArr = [], disctitleArr = [], genres = [], years = []
		function getAlbumCover() {
			if (albums.length === 0) return
			let lastAlbum = albums[albums.length - 1]
			lastAlbum.cover = getMostFrequentValue(coverArr)
			coverArr = []
			if (lastAlbum.cover) return
			if (lastAlbum.discs[0].disccover) {
				lastAlbum.cover = lastAlbum.discs[0].disccover
				return
			}
			const coversFiles = klawSync(config.collectionPath + path.dirname(lastAlbum.discs[0].tracks[0].path), {
				traverseAll: true, filter: item => {
					const ext = path.extname(item.path).toLowerCase().substring(1)
					return ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'svg', 'webp'].indexOf(ext) !== -1
				}
			})
			if (coversFiles.length === 0) return
			function getFavorite(favorites) {
				for (const c of favorites) for (const f of coversFiles) {
					if (c === f.path.toLowerCase()) {
						lastAlbum.cover = f.path
						return
					}
				}
			}
			getFavorite(['cover.jpg', 'cover.jpeg', 'cover.png', 'cover.gif', 'cover.bmp', 'cover.svg', 'cover.webp'])
			getFavorite(['folder.jpg', 'folder.jpeg', 'folder.png', 'folder.gif', 'folder.bmp', 'folder.svg', 'folder.webp'])
			getFavorite(['front.jpg', 'front.jpeg', 'front.png', 'front.gif', 'front.bmp', 'front.svg', 'front.webp'])
			//si ni cover, folder et front, on choisit l'image la plus lourde
			const c = coversFiles.sort((a, b) => (a.stats.size > b.stats.size) ? -1 : (a.stats.size === b.stats.size) ? 0 : 1)[0]
			lastAlbum.cover = c.path
		}

		for (let m of musics) {
			const track = {
				artists: m.artists,
				duration: m.duration,
				genres: m.genres,
				path: m.path,
				title: m.title,
				track: m.track,
				year: m.year,
				lyrics: m.lyrics
			}
			const disc = {
				disc: m.disc,
				tracks: [track]
			}
			const album = {
				album: m.album,
				artwork: m.artwork,
				discs: [disc]
			}
			const i = albums.findIndex(a => a.artwork === m.artwork && a.album === m.album)
			if (i === - 1) { // album pas encore créé : on l'ajoute, avec le disque et la piste actuelle...
			 	//... mais avant on finalise le dernier album créé en lui offrant un clef cover
				getAlbumCover()
				if (albums.length > 0) {
					const lastAlbum = albums[albums.length - 1]
					const lastDisc = lastAlbum.discs[lastAlbum.discs.length - 1]
					lastDisc.disccover = getMostFrequentValue(disccoverArr)
					if (lastAlbum.discs.length === 1) lastAlbum.discs[0].disccover = undefined
					lastDisc.disctitle = getMostFrequentValue(disctitleArr)
					disccoverArr = []
					disctitleArr = []
				}
				albums.push(album)
			} else { // album déjà créé : on y ajoute la piste actuelle, dans le disque présent ou un nouveau
				let a = albums[i]
				const j = a.discs.findIndex(disc => disc.disc === m.disc)
				if (j === -1) { // disque pas encore créé : on l'ajoute, avec la piste actuelle...
					if (a.discs.length > 0) { //... mais avant, si on a déjà un disque, on lui ajoute des clefs (disccover, disctitle)
						const lastDisc = a.discs[a.discs.length - 1]
						lastDisc.disccover = getMostFrequentValue(disccoverArr)
						lastDisc.disctitle = getMostFrequentValue(disctitleArr)
						disccoverArr = []
						disctitleArr = []
					}
					a.discs.push(disc)
				} else { // disque déjà créé : on y ajoute la piste actuelle
					a.discs[j].tracks.push(track)
				}
			}
			// remplissage des tableaux coverArr (album), disccoverArr et disctitleArr (disque), genres et years (global)
			if (m.pictures) {
				for (let p of m.pictures) {
					const pix = `data:${p.format};base64,${p.data.toString('base64')}`
					p.type === 'Cover (front)' ? coverArr.push(pix)	: disccoverArr.push(pix)
				}
			}
			if (m.disctitle) disctitleArr.push(m.disctitle)
			if (m.genres) {
				for (let g of m.genres) {
					if (genres.indexOf(g) === -1) genres.push(g)
				}
			}
			if (m.year && years.indexOf(m.year) === -1) years.push(m.year)
		}
		getAlbumCover() //permet d'ajouter l'image au dernier album

		// albums par œuvre, puis par album
		albums = albums.sort((a, b) => (a.artwork > b.artwork) ? 1 : (a.artwork === b.artwork) ? ((a.album > b.album) ? 1 : -1) : -1)
		for (let a of albums) { // disques par num
			a.discs = a.discs.sort((a, b) => (a.disc > b.disc) ? 1 : (a.disc === b.disc) ? 0 : -1)
			for (let d of a.discs) { // pistes par num
				d.tracks = d.tracks.sort((a, b) => (a.track > b.track) ? 1 : (a.track === b.track) ? 0 : -1)
			}
		}
		albums = albums.sort((a, b) => a.artwork.localeCompare(b.artwork, 'fr', {ignorePunctuation: true}))
		years = years.sort((a, b) => a - b)
		genres = genres.sort((a, b) => a.localeCompare(b, 'fr', {ignorePunctuation: true}))

		// lists
		for (let i=0, nb=albums.length; i < nb; i++) {
			const a = albums[i]
			for (let j=0, nb1=a.discs.length; j < nb1; j++) {
				const d = a.discs[j]
				for (let k=0, nb2=d.tracks.length; k < nb2; k++) {
					for (let l of lists) {
						const t = d.tracks[k]
						const index = l.t.indexOf(t.path.normalize())
						if (index !== -1) l.t[index] = [i, j, k]
					}
				}
			}
		}
		for (let l of lists) {
			const tracks = []
			for (let t of l.t) {
				if (typeof t === 'string') {
					logPath.push(t)
				} else tracks.push(t)
			}
			l.t = tracks
		}

		// Création/suppression des logs
		const logTracksPath = path.join(config.collectionPath, 'tracks.log')
		const logListsPath = path.join(config.collectionPath, 'play.lists.log')
		let text = ''
		if (logPath.length > 0) {
			text = 'Path unknown\n\n'
			text += logPath.join('\n')
		}
		if (logSyntax.length > 0) {
			text += '\n\n'
			text += 'Bad syntax error\n\n'
			text += logSyntax.join('\n')
			text += '\n\n\n\nShow Musiko documentation for help (https://bitbucket.org/diatomee/musiko/src/master/README.md).'
		}
		if (logPath.length > 0 || logSyntax.length > 0) {
			fs.writeFileSync(logListsPath, text)
			//alert('Errors in play.lists file. Show "' + logListsPath + '" for more details.')
		} else if (fs.existsSync(logListsPath)) fs.unlinkSync(logListsPath)
		if (logMusics.length > 0) {	
			text = 'Metadata album, artwork or title are required for these tracks :\n' + logMusics.join('\n')
			fs.writeFileSync(logTracksPath, text)
			//alert('Tracks excluded: ' + logMusics.length + '. Show "' + logTracksPath + '" for more details.')
		} else if (fs.existsSync(logTracksPath)) fs.unlinkSync(logTracksPath)
		

		if (!fs.existsSync(savePATH)) fs.mkdirSync(savePATH)
		fs.writeFileSync(path.join(savePATH, 'collection.json'), JSON.stringify(albums))
		fs.writeFileSync(path.join(savePATH, 'playlists.json'), JSON.stringify(lists))
		fs.writeFileSync(path.join(savePATH, 'genres.json'), JSON.stringify(genres))
		fs.writeFileSync(path.join(savePATH, 'years.json'), JSON.stringify(years))
		fs.writeFileSync(path.join(savePATH, 'lastAlbums.json'), JSON.stringify([]))
		if (!fs.existsSync(path.join(savePATH, 'config.json'))) {
			fs.writeFileSync(path.join(savePATH, 'config.json'), JSON.stringify(config))
		}
		win.webContents.send('finish', {})
	}
	let lists = [], logSyntax = [], logPath = []
	if (fs.existsSync(path.join(config.collectionPath, 'play.lists'))) {
		// Récup dans un tableau toutes les playlists et leurs pistes et leur image.
		// Sera complété dans parseFiles et si des chemins sont introuvés, d'autres erreurs seront dans le log
		let content = fs.readFileSync(path.join(config.collectionPath, 'play.lists'), 'utf-8').split('\n')
		let nbLine = 0
		for (let line of content) {
			nbLine ++
			line = line.trim()
			const first = line.substring(0, 1)
			if (first === '#') {
				lists.push({n: line.substring(1), t: []})
			} else if (first === '/') {
				lists[lists.length - 1].t.push(line)
			} else if (first === '>') {
				lists[lists.length - 1].cover = config.collectionPath + line.substring(1)
			} else if (line !== '') logSyntax.push('#' + nbLine + ': ' + line)
		}
	}
	extendAudioFiles(klaw(config.collectionPath, audio_exts))
})