# Versions

## 1.0.6 (current / actuelle)

**EN**

1. Choice of 10 themes to change the look.
2. New panel: Artists.
3. Improved research.
4. Panel removal: Filters.
5. Various bugs fixed.
6. Adding this small update window ;)

**FR**

1. Choix parmi 10 thèmes pour changer l'apparence.
2. Nouveau panneau : Artistes.
3. Amélioration de la recherche.
4. Suppression du panneau : Filtres.
5. Divers bogues corrigés.
6. Ajout de cette petite fenêtre de mise à jour ;)

## 1.0.5

**FR**

1. La création de la collection musicale est plus rapide et se fait jusqu'au bout.
2. Si la langue de l'utilisateur est indisponible, l'anglais sera mis par défaut.
3. Parmi plusieurs pochettes disponibles dans le dossier ou le sous-dossier où se trouve la musique, le choix se portera prioritairement sur :
	1. cover.[ext]
	2. folder.[ext]
	3. front.[ext]
	4. [max-file-size].[ext]
4. Lors d'une mise à jour, la collection n'est plus rafraîchit systématiquement.
5. Recherche améliorée :
	- saisir ":" + lettre = accéder à l'index dans la liste des albums
	- commencer à saisir du texte n'importe où pour débuter une recherche
	- les résultats de recherche sont cliquables directement pour être joués ou observés.
