🡣 Documentation: English - Français

[![musiko](https://snapcraft.io//musiko/badge.svg)](https://snapcraft.io/musiko)

# ENGLISH

Musiko is a free to use, [open source](https://bitbucket.org/diatomee/musiko/src/master/) and cross platform **music player**.

## Download

* [Linux (snap)](https://snapcraft.io/musiko)
* [Other (dmg, exe, appImage)](https://bitbucket.org/diatomee/musiko/downloads/)

## Features

* Multiple audio formats (mp3, mp4, flac, wav, ogg, wma and more);
* Display music by albums;
* Totally Web disconnected (no request sent);
* Recognize a lot of metadata (including lyrics, rating, disc subtitle);
* Search and filters;
* Playlists support.

## Supported metadata

* year (release year)
* track (track number)
* disk (disc number)
* title (track title)
* album (album title)
* picture ("cover (front)" for album, other type for disc)
* synchronized lyrics (but used in one text)
* discsubtitle (disc title)
* albumartist (used as artwork name in Musiko)
* artist (for multiples artists, use "," as separator)
* genre (for multiples genres, use "," as separator)

Use ID3v2.3 or more be able to use all these tags. [Kid3](https://kid3.sourceforge.io/) is a good tag editor. If you wish more supported metadata, please [create issue](https://bitbucket.org/diatomee/musiko/issues/new) and explain your needs.

## Use

Musiko has a minimal interface. A left click on the album cover, the cover of a disc or the title of a track adds the music to the queue. If it is empty, the lead track is played. Right-clicking on an album cover or track title displays the album or track information. Right click can be replaced by Ctrl + left click.

### Add playlists

At the root of the music folder (the one indicated in the settings), you must create a file called "play.lists". This will contain all of the playlists.

Content example  :

```
#Chanteuses japonaises
>/chanteusesjaponaises.jpg
/Yoshiko Sai/Taiji no yume (1977)/10-胎児の夢.mp3
/Miharu Koshi/Chanson solaire (1995)/12-Parlez-moi d'amour.mp3
/Hako Yamasaki/Tobimasu/07 See No Shadows_影が見えない [Kage Ga Mienai].mp3
/Junko Kudo - Akane Iro No Carnival/8. すみれ・チューリップ.mp3
#Mix classique
>/monmix.png
/Debussy/La mer.flac
/Autre/Amarilli.mp3
/Camille de St Saens/danse-macabre.mp3
```

A playlist consists, in order, of a **title** (introduced by #), a **link to a cover** (introduced by >), then the list of **links to tracks**. The example above has two playlists. The first includes 4 tracks and the second includes 3 tracks.

### Add lyrics

The lyrics are added to the choice, in:

* music metadata ("synchronized lyrics" field, although they will not be synchronized);
* a file with the name of the music, with the extension `.lyrics` to be inserted into the music folder or sub-folder. If the file is named `03. my_track.mp3`, the associated lyrics file is named `03. my_track.lyrics`.

If the lyrics are in both the metadata and in a file, the lyrics in the file will be used.

## Technical (for developer)

Musiko uses Javascript, Electron, VueJS, the music-metadata module and a few others for its development. It organizes the tracks of a folder and its sub-folders into albums. Here is the structure of an album:

```
{
	album,
	artwork,
	cover,
	discs: [{
		disc,
		disccover,
		disctitle,
		tracks: [{
			artists: [],
			duration,
			genres: [],
			path,
			title,
			track,
			year,
			lyrics
		},...]
	},...]
}
```

---

# FRANÇAIS

Musiko est un **lecteur de musique** gratuit, [open source](https://bitbucket.org/diatomee/musiko/src/master/) et multi-OS.

## Téléchargements

* [Linux (snap)](https://snapcraft.io/musiko)
* [Autres (dmg, exe, appImage)](https://bitbucket.org/diatomee/musiko/downloads/) 

## Fonctionnalités

* Multiple formats audio (mp3, mp4, flac, wav, ogg, wma et plus);
* Affiche les musiques par albums;
* Totalement déconnecté du Web (aucune requête n'est envoyée);
* Reconnait un grand nombre de métadonnées (y compris les paroles, le classement ou le titre d'un disque d'un album);
* Recherche et filtres;
* Support des listes de lecture.

## Métadonnées supportées

* année (année de sortie)
* piste (numéro de piste)
* disque (numéro de disque)
* titre (titre de la piste)
* album (titre de l'album)
* image ("pochette (avant)" pour un album, autre type pour un disque)
* paroles synchronisée (utilisées en non-synchronisée dans Musiko)
* sous-titre de disque (titre du disque)
* artiste de l'album (utilisé comme nom d'oeuvre dans Musiko)
* artiste (pour plusieurs artistes, utiliser "," comme séparateur)
* genre (pour plusieurs genres, utiliser "," comme séparateur)

Il faut utiliser ID3v2.3 (ou version supérieure) afin d'accéder à toutes ces métadonnées. [Kid3](https://kid3.sourceforge.io/) est un bon éditeur de métadonnées. Si tu souhaites que Musiko supporte d'avantage de métadonnées, tu peux [faire une demande](https://bitbucket.org/diatomee/musiko/issues/new) sur Bitbucket en expliquant tes besoins.

## Utilisation

Musiko dispose d'une interface minimale. Un clic gauche sur la pochette d'un album, la pochette d'un disque ou le titre d'une piste ajoute la ou les musiques à la file d'attente. Si celle-ci est vide, la piste en tête se joue. Un clic droit sur la pochette d'un album ou le titre d'une piste permet d'afficher les informations de l'album ou de la piste. Le clic droit peut être remplacé par Ctrl + clic gauche.

### Ajout de listes de lectures

À la racine du dossier des musiques (celui indiqué dans les paramètres), il faut créer un fichier nommé "play.lists". Celui-ci contiendra toutes les listes de lecture.

Voici un exemple de contenu :

```
#Chanteuses japonaises
>/chanteusesjaponaises.jpg
/Yoshiko Sai/Taiji no yume (1977)/10-胎児の夢.mp3
/Miharu Koshi/Chanson solaire (1995)/12-Parlez-moi d'amour.mp3
/Hako Yamasaki/Tobimasu/07 See No Shadows_影が見えない [Kage Ga Mienai].mp3
/Junko Kudo - Akane Iro No Carnival/8. すみれ・チューリップ.mp3
#Mix classique
>/monmix.png
/Debussy/La mer.flac
/Autre/Amarilli.mp3
/Camille de St Saens/danse-macabre.mp3
```

Une liste de lecture se compose, dans l'ordre, d'un **titre** (introduit par #), d'un **lien vers une pochette** (introduit par >), puis de la liste des **liens vers les musiques**. L'exemple ci-dessus comporte deux listes de lecture. La première comprend 4 musiques et la seconde en comprend 3.

### Ajout de paroles

Les paroles s'ajoutent au choix, dans :

* les métadonnées de la musique (champ "paroles synchronisée", bien qu'elles ne seront pas synchronisées);
* un fichier portant le nom de la musique, avec l'extension `.lyrics` à insérer dans le dossier ou le sous-dossier de la musique. Si le fichier se nomme `03. ma_piste.mp3`, le fichier de paroles associées se nomme `03. ma_piste.lyrics`.

Si les paroles se trouvent à la fois dans les métadonnées et dans un fichier, celles du fichier seront utilisées.

## Technique (pour développeur)

Musiko utilise pour son développement Javascript, Electron, VueJS, le module music-metadata et quelques autres. Il organise les pistes d'un dossier et de ses sous-dossier en albums. Voici la structure d'un album :

```
{
	album,
	artwork,
	cover,
	discs: [{
		disc,
		disccover,
		disctitle,
		tracks: [{
			artists: [],
			duration,
			genres: [],
			path,
			title,
			track,
			year,
			lyrics
		},...]
	},...]
}
```
