const
	{app} = require('electron').remote,
	{ipcRenderer, shell, remote, BrowserWindow} = require('electron'),
	path = require('path'),
	fs = require('fs'),
	BASELINE = '<em>La muziko estas grandioza.</em>',
	savePATH = path.join(app.getPath('appData'), 'musiko'),
	VERSION = app.getVersion(), // version in package.json
	isLinux = process.platform === 'linuxgood',
	currentWindow = remote.getCurrentWindow()

//Lancement de l'application. Récupère pour cela les JSONs dans savePATH
function launchApp() {
	function getContent(filename, defaultReturn = []) {
		const p = path.join(savePATH, filename + '.json')
		if (fs.existsSync(p)) return JSON.parse(fs.readFileSync(p))
		return defaultReturn
	}
	const
		albums = getContent('collection'),
		lists = getContent('playlists').map(l => ({
			album: l.n,
			cover: l.cover,
			artwork: '❤',
			discs: [{
				disc: 0,
				tracks: l.t.map(arrADT => { //une piste est une réf à un num d'Album, à un num Disc et à un num Track
					let a = arrADT[0], d = arrADT[1], t = arrADT[2]
					a = albums[a]
					d = a.discs[d]
					t = d.tracks[t]
					const cloneT = { ...t } //façon de cloner un objet (spread)
					cloneT.albumRef = a
					cloneT.discRef = d
					return cloneT
				})
			}]
		})),
		genres = getContent('genres'),
		years = getContent('years'),
		lastAlbums = getContent('lastAlbums'),
		config = getContent('config', {})
	
	document.body.setAttribute('data-theme', config.theme)
	const a = {
		configPath: savePATH,
		language: config.language,
		theme: config.theme,
		L: lang[config.language],
		isShutdown: false,
		allAlbums: albums, albums, lists, genres, years,
		lastAlbums,
		nbLastAlbums: config.nbLastAlbums,
		collectionPath: config.collectionPath,
		tracks: [],
		queue: [], past: [],
		defaultTitle: BASELINE,
		currentTitle: BASELINE,
		showPause: false,
		audio: undefined,
		inPan: undefined, //musique affichée dans le panneau gauche.
		currentQPlayed: undefined,
		shuffle: arr => arr.map(a => [Math.random(), a]).sort((a, b) => a[0] - b[0]).map(a => a[1]),
		getTime: function (t) {
			let
				h = Math.floor(t / 3600),
				m  = Math.floor((t % 3600) / 60),
				s  = Math.floor(t % 60)
			if (s < 10) s = '0' + s
			if (m < 10) m = '0' + m
			const res = (h ? h + ':' : '') + m + ':' + s
			if (res === 'NaN:NaN') return '-:-'
			return res
		},
		Sound: function () {
			if (this.queue.length === 0) {
				this.currentTitle = this.defaultTitle
				document.title = this.currentTitle === this.defaultTitle ? 'Musiko' : this.currentTitle
				this.audio = undefined
				this.showPause = false
				if (this.isShutdown) {
					let counter = 10
					const secs = 1
					const c = setInterval(() => {
						counter --
						document.title = '⚡ ' + counter
						if (counter === 0) {
							document.title = 'Adiaŭ !'
							clearInterval(c)
						}
						if (!this.isShutdown) {
							clearInterval(c)
							document.title = this.currentTitle === this.defaultTitle ? 'Musiko' : this.currentTitle
						}
					}, secs * 1000)
					const i = setInterval(() => {
						if (this.isShutdown) ipcRenderer.sendSync('go-shutdown', 'go')
						clearInterval(i)
					}, counter * 1000 + secs * 1000)
				}
				setTimeout(() => document.getElementById('timeline').style.width = '0px', 100)
				return
			}
			const t = this.queue[0].track
			this.audio = new Audio()
			this.audio.src = this.collectionPath + t.path
			this.audio.load()
			this.currentTitle = t.title +  ' — ' + t.artists.join(', ')
			this.inPan = this.queue[0]
			this.currentQPlayed = this.queue[0]
	
			//sauvegarde de l'album des dernières écoutes
			const newAlbum = this.currentQPlayed.album
			let notPlaylist = true
			for (let list of lists) {
				if (list.cover === newAlbum.cover) {
					notPlaylist = false
					break
				}
			}
			if (notPlaylist) {
				let la = this.lastAlbums.filter(
					alb => alb.album !== newAlbum.album && alb.artwork !== newAlbum.artwork
				)
				la.unshift(newAlbum)
				la = la.slice(0, this.nbLastAlbums)
				this.lastAlbums = la
				fs.writeFileSync(path.join(savePATH, 'lastAlbums.json'), JSON.stringify(this.lastAlbums))
			}
	
			document.title = this.currentTitle
			this.past.unshift(this.queue.shift())
	
			this.showPause = true
			this.audio.play()
			this.audio.addEventListener('ended', function() {
				a.audio = undefined
				a.Sound()
			}, false)
			this.audio.addEventListener('timeupdate', function() {
				//this = audio
				if (!this) return
				let t = a.getTime(this.duration - this.currentTime)
				if (t === 'NaN:NaN') return
				if (a.progressValue !== t) {
					document.title = a.currentTitle + ' (' + t + ')'
					a.progressValue = t
				}
				const w = `calc(100% * ${this.currentTime} / ${this.duration})`
				document.getElementById('timeline').style.width = w
			}, false)
		},
		playAlbum: function (album, discIndex) {
			if (!discIndex) {
				for (let d of album.discs) for (let track of d.tracks) {
					this.queue.push({track, album})
				}
			} else {
				for (let track of album.discs[discIndex].tracks) {
					this.queue.push({track, album})
				}
			}
			if (!this.audio) this.Sound()
		},
		playTrack: function (q) {
			this.queue.push(q)
			if (!this.audio) this.Sound()
		},
		playPause: function () {
			if (!this.audio && this.queue.length === 0) {
				// joue un album parmi ceux affichés, choisi aléatoirement
				const albums = this.albums
				const album = albums[Math.floor(Math.random() * albums.length)]
				for (let disc of album.discs) for (let track of disc.tracks) {
					this.queue.push({track, album})
				}
				// joue la première musique de queue
				this.Sound()
				return
			}
			if (!this.audio.paused) {
				// met en pause la lecture
				this.audio.pause()
				this.showPause = false
				return
			}
			// reprend la lecture
			this.audio.play()
			this.showPause = true
		},
		stop: function () {
			if (this.audio) {
				this.audio.pause()
				this.audio.currentTime = 0
				this.showPause = false
			}
		},
		random: function () {
			if (this.queue.length === 0) {
				// crée une queue de 50 musiques à partir de tous les albums
				for (let a of this.albums) for (let d of a.discs) for (let t of d.tracks) {
					this.queue.push({track: t, album: a})
				}
				this.queue = this.shuffle(this.queue).slice(0, 50)
				if (!this.audio) {
					// crée l'audio et le joue
					this.Sound()
					return
				}
				return
			}
			// mélange la queue existante
			this.queue = this.shuffle(this.queue)
		},
		skip: function () {
			if (this.audio) {
				this.audio.pause()
				this.Sound()
			}
		}
	}
	if (a.albums.length !== 0) {
		const rdmAlbum = a.shuffle(a.albums)[0]
		const rdmDisk = a.shuffle(rdmAlbum.discs)[0]
		const rdmTrack = a.shuffle(rdmDisk.tracks)[0]
		a.inPan = {track: rdmTrack, album: rdmAlbum}
	}
	const vm = new Vue({
		el: "#app",
		components: {Banner, Asidepan, Album},
		data: {
			a,
			I: icons,
			tab: "Album",
			btAction: "Play",
			tabs: ["Queue", "Search", "Album", "Track", "Artist", "Params"]
		}
	})
	return vm
}

// INIT : Cette fonction génère la collection en prenant en compte une ancienne config si elle est dispo. À la fin de la génération, on obtient plusieurs JSONs dans savePATH et l'app est relancée (ce qui ait qu'on revient au routage).
function initCollection(configRecup) {
	let config, isNewVersion = false
	function getLang() {
		const l = ['fr', 'de', 'it', 'ae', 'ru', 'es', 'jp', 'cn']
		const i = l.indexOf(app.getLocale().slice(0, 2))
		return i > -1 ? l[i] : 'en'
	}
	if (!configRecup && fs.existsSync(path.join(savePATH, 'config.json'))) {
		const {collectionPath, language, theme, nbLastAlbums} = JSON.parse(fs.readFileSync(path.join(savePATH, 'config.json')))
		config = {collectionPath, language, theme, nbLastAlbums}
	} else if (configRecup) {
		config = configRecup
		isNewVersion = configRecup.version !== VERSION
	} else {
		config = {
			collectionPath: app.getPath('music'),
			nbLastAlbums: 9,
			language: getLang(),
			theme: 'dark'
		}
	}
	const note = `
		<h1>Updated to Musiko ${VERSION}</h1>
		<ol>
			<li>Choice of 10 themes to change the look.</li>
			<li>New panel: Artists.</li>
			<li>Improved research.</li>
			<li>Panel removal: Filters.</li>
			<li>Various bugs fixed.</li>
			<li>Adding this small update window ;)</li>
		</ol>
		<p><em>The release notes are accessible via the Settings panel, by clicking on the current version number.</em></p>
		<div class=bt id=ok>OK</div>
	`
	config.version = VERSION
	document.body.setAttribute('data-theme', config.theme)
	const L = lang[config.language]
	const splashload = document.createElement('div')
	splashload.classList.add('splashload')
	splashload.innerHTML = L.creationCollection + `<div id=loader></div>`
	if (isNewVersion) splashload.innerHTML += `<div class=versionNote id=vn>${note}</div>`
	document.body.appendChild(splashload)
	const loader = document.getElementById('loader')
	if (isNewVersion) {
		document.getElementById('ok').addEventListener('click', () => {
			isNewVersion = false
			splashload.removeChild(document.getElementById('vn'))
		})
	}
	ipcRenderer.on('info' , (e, data) => {
		loader.innerHTML = data.nb + ' / ' + data.total + ' ' + L.musiquesAnalysees
	})
	ipcRenderer.on('finish', (e, data) => {
		if (isNewVersion && document.getElementById('ok')) {
			//c'est une nouvelle version. L'app ne se relancera que si on clique sur OK.
			document.getElementById('ok').addEventListener('click', () => currentWindow.reload())
		} else currentWindow.reload()
	})
	ipcRenderer.send('initCollection', config, savePATH)
}

// ROUTAGE : Si on a un config.json, on lance l'app normalement, sinon on initialise la collection
let vm
if (fs.existsSync(path.join(savePATH, 'config.json'))) {
	const configRecup = JSON.parse(fs.readFileSync(path.join(savePATH, 'config.json')))
	if (configRecup.version !== VERSION) { // Maj détectée
		fs.rmdirSync(savePATH, { recursive: true })
		initCollection(configRecup)
		//vm = launchApp()
	} else {
		vm = launchApp()
	}
} else initCollection()


let searchByIndex = false
document.addEventListener('keydown', function (e) {
	if (e.which === 112) {
		window.open('https://bitbucket.org/diatomee/musiko/src/master/README.md', '_blank', 'nodeIntegration=no')
	} else {
		if (e.target.tagName === 'BODY' && vm) {
			console.log(e.which)
			if (e.which === 191) {
				searchByIndex = true
				setTimeout(() => searchByIndex = false, 5000)
			} else if (searchByIndex) {
				const asideSearch = vm.$options.components.Asidepan.components.AsideSearch.methods
				const letter = String.fromCharCode(e.which)
				function getIndexs() {
					let chars = []
					for (const a of vm.$root.a.albums) {
						const c = a.artwork.split('')[0].toUpperCase()
						if (chars.indexOf(c) === -1) chars.push(c)
					}
					return chars
				}
				const indexs = getIndexs()
				if (indexs.indexOf(letter) !== -1) asideSearch.goTo(letter)
			} else vm.$root.tab = 'Search'
		}
	}
})